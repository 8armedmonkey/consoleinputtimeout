package com.eightam.consoleinputtimeout;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;

class ConsoleInputReadTask implements Callable<String> {

    private BufferedReader reader;
    private AtomicBoolean closed;

    ConsoleInputReadTask(InputStream inputStream) {
        reader = new BufferedReader(new InputStreamReader(inputStream));
        closed = new AtomicBoolean(false);
    }

    @Override
    public String call() throws Exception {
        while (!reader.ready()) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                // Ignore error.
            }
        }
        return reader.readLine();
    }

    void close() {
        if (reader != null) {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        closed.set(true);
    }

    boolean isClosed() {
        return closed.get();
    }

}
