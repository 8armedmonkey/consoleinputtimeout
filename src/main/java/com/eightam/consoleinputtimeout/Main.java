package com.eightam.consoleinputtimeout;

import java.util.concurrent.TimeoutException;

/**
 * Original solution could be found here:
 * https://www.javaspecialists.eu/archive/Issue153.html
 */
public class Main {

    private static final String RUN_CONSOLE_INPUT = "--with-console-input";

    public static void main(String[] args) {
        if (args.length > 0 && RUN_CONSOLE_INPUT.equals(args[0])) {
            runWithConsoleInput();
        } else {
            runWithConsoleInput2();
        }
    }

    private static void runWithConsoleInput() {
        ConsoleInput input = new ConsoleInput(5000);

        for (int i = 0; i < 3; i++) {
            System.out.println("Enter any input: ");

            try {
                String line = input.readLine();
                System.out.println("You entered: " + line);

            } catch (TimeoutException e) {
                System.out.println("You have not entered anything.");
            }
        }

        input.destroy();
    }

    private static void runWithConsoleInput2() {
        ConsoleInput2 input = new ConsoleInput2();
        CancellingThread t = new CancellingThread(input);

        t.start();

        for (int i = 0; i < 3; i++) {
            System.out.println("Enter any input: ");

            try {
                String line = input.readLine();
                System.out.println("You entered: " + line);

            } catch (Exception e) {
                System.out.println("Input is interrupted.");
            }
        }

        t.interrupt();

        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Cancelling and destroying input.");

        input.cancel();
        input.destroy();
    }

    static class CancellingThread extends Thread {

        private ConsoleInput2 input;
        private boolean shouldFinish;

        CancellingThread(ConsoleInput2 input) {
            this.input = input;
        }

        @Override
        public void run() {
            super.run();

            while (!shouldFinish) {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    shouldFinish = true;
                }

                input.cancel();
            }

            input = null;
        }

    }

}
