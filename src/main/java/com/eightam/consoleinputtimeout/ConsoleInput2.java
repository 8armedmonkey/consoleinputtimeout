package com.eightam.consoleinputtimeout;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

class ConsoleInput2 {

    private ExecutorService executorService;
    private ConsoleInputReadTask inputReadTask;
    private Future<String> result;

    ConsoleInput2() {
        executorService = Executors.newSingleThreadExecutor();
    }

    String readLine() throws Exception {
        System.out.println("READ LINE");

        inputReadTask = new ConsoleInputReadTask(System.in);
        result = executorService.submit(inputReadTask);

        return result.get();
    }

    void cancel() {
        System.out.println("CANCEL");

        if (result != null && !result.isCancelled()) {
            result.cancel(true);
        }

        if (inputReadTask != null && !inputReadTask.isClosed()) {
            inputReadTask.close();
        }
    }

    void destroy() {
        executorService.shutdownNow();
    }

}
