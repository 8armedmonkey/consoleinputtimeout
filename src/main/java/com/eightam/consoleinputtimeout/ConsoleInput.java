package com.eightam.consoleinputtimeout;

import java.util.concurrent.*;

class ConsoleInput {

    private ExecutorService executorService;
    private long timeoutMillis;

    ConsoleInput(long timeoutMillis) {
        this.executorService = Executors.newSingleThreadExecutor();
        this.timeoutMillis = timeoutMillis;
    }

    String readLine() throws TimeoutException {
        try {
            Future<String> result = executorService.submit(new ConsoleInputReadTask(System.in));
            return result.get(timeoutMillis, TimeUnit.MILLISECONDS);

        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    void destroy() {
        executorService.shutdownNow();
    }

}
